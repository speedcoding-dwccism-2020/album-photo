function removeClassBadgeActive() {
  $(".badge-active").addClass("badge-inactive");
  $(".badge-active").removeClass("badge-active");
}

function getAll() {
  $(".catAll").hide();
  $(".catAll").show();
  removeClassBadgeActive();
  $("#badgeAll").addClass("badge-active");
}

function getCatAffaire() {
  $(".catAll").hide();
  $(".catAffaire").show();
  removeClassBadgeActive();
  $("#badgeAffaire").addClass("badge-active");
}

function getCatBatiment() {
  $(".catAll").hide();
  $(".catBatiment").show();
  removeClassBadgeActive();
  $("#badgeBatiment").addClass("badge-active");
}

function getCatJardin() {
  $(".catAll").hide();
  $(".catJardin").show();
  removeClassBadgeActive();
  $("#badgeJardin").addClass("badge-active");
}

function getCatOmbre() {
  $(".catAll").hide();
  $(".catOmbre").show();
  removeClassBadgeActive();
  $("#badgeOmbre").addClass("badge-active");
}

function getCatReligion() {
  $(".catAll").hide();
  $(".catReligion").show();
  removeClassBadgeActive();
  $("#badgeReligion").addClass("badge-active");
}